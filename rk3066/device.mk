#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
ifeq ($(TARGET_PREBUILT_KERNEL),)
LOCAL_KERNEL := device/rockchip/rk3066/kernel
else
LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
endif
PRODUCT_COPY_FILES += \
	device/rockchip/rk3066/root/init:root/init \
	device/rockchip/rk3066/root/init.rc:root/init.rc \
	device/rockchip/rk3066/root/init.rk30board.rc:root/init.rk30board.rc \
	device/rockchip/rk3066/root/init.rk30board.usb.rc:root/init.rk30board.usb.rc \
	device/rockchip/rk3066/root/ueventd.rk30board.rc:root/ueventd.rk30board.rc \
	device/rockchip/rk3066/root/rk30xxnand_ko.ko.3.0.8+:root/rk30xxnand_ko.ko.3.0.8+ \
	device/rockchip/rk3066/root/sbin/e2fsck:root/sbin/e2fsck \
	device/rockchip/rk3066/root/sbin/mkdosfs:root/sbin/mkdosfs \
	device/rockchip/rk3066/root/sbin/mke2fs:root/sbin/mke2fs \
	device/rockchip/rk3066/root/sbin/readahead:root/sbin/readahead \
	device/rockchip/rk3066/root/sbin/resize2fs:root/sbin/resize2fs
#firmware
PRODUCT_COPY_FILES += \
	device/rockchip/rk3066/system/etc/firmware/athtcmd_ram.bin:system/etc/firmware/athtcmd_ram.bin \
	device/rockchip/rk3066/system/etc/firmware/athwlan.bin.z77:system/etc/firmware/athwlan.bin.z77 \
	device/rockchip/rk3066/system/etc/firmware/bdata.SD31.bin:system/etc/firmware/bdata.SD31.bin \
	device/rockchip/rk3066/system/etc/firmware/data.patch.hw2_0.bin:system/etc/firmware/data.patch.hw2_0.bin \
	device/rockchip/rk3066/system/etc/firmware/device.bin:system/etc/firmware/device.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_bcm4319.bin:system/etc/firmware/fw_bcm4319.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_bcm4329_apsta.bin:system/etc/firmware/fw_bcm4329_apsta.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_bcm4329.bin:system/etc/firmware/fw_bcm4329.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_bcm4329_wapi.bin:system/etc/firmware/fw_bcm4329_wapi.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_bcm4330_apsta.bin:system/etc/firmware/fw_bcm4330_apsta.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_bcm4330.bin:system/etc/firmware/fw_bcm4330.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK901a0_apsta.bin:system/etc/firmware/fw_RK901a0_apsta.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK901a0.bin:system/etc/firmware/fw_RK901a0.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK901a2_apsta.bin:system/etc/firmware/fw_RK901a2_apsta.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK901a2.bin:system/etc/firmware/fw_RK901a2.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK901a2_p2p.bin:system/etc/firmware/fw_RK901a2_p2p.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK901.bin:system/etc/firmware/fw_RK901.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK903b2_apsta.bin:system/etc/firmware/fw_RK903b2_apsta.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK903b2.bin:system/etc/firmware/fw_RK903b2.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK903b2_p2p.bin:system/etc/firmware/fw_RK903b2_p2p.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK903.bin:system/etc/firmware/fw_RK903.bin \
	device/rockchip/rk3066/system/etc/firmware/fw_RK903_p2p.bin:system/etc/firmware/fw_RK903_p2p.bin \
	device/rockchip/rk3066/system/etc/firmware/nvram_4330.txt:system/etc/firmware/nvram_4330.txt \
	device/rockchip/rk3066/system/etc/firmware/nvram_B23.txt:system/etc/firmware/nvram_B23.txt \
	device/rockchip/rk3066/system/etc/firmware/nvram_RK901.bad:system/etc/firmware/nvram_RK901.bad \
	device/rockchip/rk3066/system/etc/firmware/nvram_RK901.txt:system/etc/firmware/nvram_RK901.txt \
	device/rockchip/rk3066/system/etc/firmware/nvram_RK903_26M.cal:system/etc/firmware/nvram_RK903_26M.cal \
	device/rockchip/rk3066/system/etc/firmware/nvram_RK903.cal:system/etc/firmware/nvram_RK903.cal \
	device/rockchip/rk3066/system/etc/firmware/nvram_RK903.txt:system/etc/firmware/nvram_RK903.txt \
	device/rockchip/rk3066/system/etc/firmware/nvram.txt:system/etc/firmware/nvram.txt \
	device/rockchip/rk3066/system/etc/firmware/otp.bin.z77:system/etc/firmware/otp.bin.z77 \
	device/rockchip/rk3066/system/etc/firmware/sd8686.bin:system/etc/firmware/sd8686.bin \
	device/rockchip/rk3066/system/etc/firmware/sd8686_helper.bin:system/etc/firmware/sd8686_helper.bin
PRODUCT_COPY_FILES += \
	device/rockchip/rk3066/system/etc/wifi/wpa_supplicant.conf:system/etc/wifi/wpa_supplicant.conf \
	device/rockchip/rk3066/system/etc/media_profiles.xml:system/etc/media_profiles.xml \
	device/rockchip/rk3066/system/etc/vold.fstab:system/etc/vold.fstab \
	device/rockchip/rk3066/system/etc/asound.conf:system/etc/asound.conf \
	device/rockchip/rk3066/system/etc/audio_policy.conf:system/etc/audio_policy.conf \
	device/rockchip/rk3066/system/etc/media_codecs.xml:system/etc/media_codecs.xml
PRODUCT_COPY_FILES += \
	device/rockchip/rk3066/system/etc/permissions/com.google.widevine.software.drm.xml:system/etc/permissions/com.google.widevine.software.drm.xml \
	device/rockchip/rk3066/system/etc/permissions/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
	device/rockchip/rk3066/system/etc/permissions/android.software.pppoe.xml:system/etc/permissions/android.software.pppoe.xml \
	device/rockchip/rk3066/system/etc/permissions/features.xml:system/etc/permissions/features.xml \
	device/rockchip/rk3066/system/etc/permissions/platform.xml:system/etc/permissions/platform.xml \
	device/rockchip/rk3066/system/etc/permissions/android.hardware.camera.autofocus.xml:system/etc/permissions/android.hardware.camera.autofocus.xml \
	device/rockchip/rk3066/system/etc/permissions/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
	device/rockchip/rk3066/system/etc/permissions/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
	device/rockchip/rk3066/system/etc/permissions/com.android.location.provider.xml:system/etc/permissions/com.android.location.provider.xml \
	device/rockchip/rk3066/system/etc/permissions/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
	device/rockchip/rk3066/system/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml \
	device/rockchip/rk3066/system/etc/permissions/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml \
	device/rockchip/rk3066/system/etc/permissions/android.hardware.touchscreen.multitouch.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.xml \
	device/rockchip/rk3066/system/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
	device/rockchip/rk3066/system/etc/permissions/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
	device/rockchip/rk3066/system/etc/permissions/android.hardware.location.xml:system/etc/permissions/android.hardware.location.xml
PRODUCT_COPY_FILES += \
	device/rockchip/rk3066/system/lib/egl/libEGL_mali.so:system/lib/egl/libEGL_mali.so \
	device/rockchip/rk3066/system/lib/egl/libGLES_android.so:system/lib/egl/libGLES_android.so \
	device/rockchip/rk3066/system/lib/egl/libGLESv1_CM_mali.so:system/lib/egl/libGLESv1_CM_mali.so \
	device/rockchip/rk3066/system/lib/egl/libGLESv2_mali.so:system/lib/egl/libGLESv2_mali.so \
	device/rockchip/rk3066/system/lib/libasound.so:system/lib/libasound.so \
	device/rockchip/rk3066/system/lib/libion.so:system/lib/libion.so \
	device/rockchip/rk3066/system/lib/libjpeghwdec.so:system/lib/libjpeghwdec.so \
	device/rockchip/rk3066/system/lib/libjpeghwenc.so:system/lib/libjpeghwenc.so \
	device/rockchip/rk3066/system/lib/libMali.so:system/lib/libMali.so \
	device/rockchip/rk3066/system/lib/libril-rk29-dataonly.so:system/lib/libril-rk29-dataonly.so \
	device/rockchip/rk3066/system/lib/librkswscale.so:system/lib/librkswscale.so \
	device/rockchip/rk3066/system/lib/libUMP.so:system/lib/libUMP.so \
	device/rockchip/rk3066/system/lib/libvpu.so:system/lib/libvpu.so \
	device/rockchip/rk3066/system/lib/libyuvtorgb.so:system/lib/libyuvtorgb.so \
	device/rockchip/rk3066/system/lib/libomxvpu_enc.so:system/lib/libomxvpu_enc.so \
	device/rockchip/rk3066/system/lib/registry:system/lib/registry \
	device/rockchip/rk3066/system/lib/libOMX_Core.so:system/lib/libOMX_Core.so \
	device/rockchip/rk3066/system/lib/libomxvpu.so:system/lib/libomxvpu.so \
	device/rockchip/rk3066/system/lib/libstagefright_foundatioo.so:system/lib/libstagefright_foundatioo.so \
	device/rockchip/rk3066/system/lib/libstagefrighu.so:system/lib/libstagefrighu.so \
	device/rockchip/rk3066/system/lib/libstagefrighthw.so:system/lib/libstagefrighthw.so \
	device/rockchip/rk3066/system/lib/libntfs-3g.so:system/lib/libntfs-3g.so \
	device/rockchip/rk3066/system/lib/modules/btusb.ko:system/lib/modules/btusb.ko \
	device/rockchip/rk3066/system/lib/modules/mali.ko:system/lib/modules/mali.ko \
	device/rockchip/rk3066/system/lib/modules/rk29-ipp.ko:system/lib/modules/rk29-ipp.ko \
	device/rockchip/rk3066/system/lib/modules/rkwifi.ko:system/lib/modules/rkwifi.ko \
	device/rockchip/rk3066/system/lib/modules/rtl8192cu.ko:system/lib/modules/rtl8192cu.ko \
	device/rockchip/rk3066/system/lib/modules/rtl8711.ko:system/lib/modules/rtl8711.ko \
	device/rockchip/rk3066/system/lib/modules/ump.ko:system/lib/modules/ump.ko \
	device/rockchip/rk3066/system/lib/modules/vpu_service.ko:system/lib/modules/vpu_service.ko \
	device/rockchip/rk3066/system/lib/modules/wlan.ko:system/lib/modules/wlan.ko \
	device/rockchip/rk3066/system/lib/soundfx:system/lib/soundfx \
	device/rockchip/rk3066/system/lib/soundfx/libaudiopreprocessing.so:system/lib/soundfx/libaudiopreprocessing.so \
	device/rockchip/rk3066/system/lib/soundfx/libbundlewrapper.so:system/lib/soundfx/libbundlewrapper.so \
	device/rockchip/rk3066/system/lib/soundfx/libdownmix.so:system/lib/soundfx/libdownmix.so \
	device/rockchip/rk3066/system/lib/soundfx/libreverbwrapper.so:system/lib/soundfx/libreverbwrapper.so \
	device/rockchip/rk3066/system/lib/soundfx/libvisualizer.so:system/lib/soundfx/libvisualizer.so \
	device/rockchip/rk3066/system/bin/displayd:system/bin/displayd \
	device/rockchip/rk3066/system/bin/akmd8975:system/bin/akmd8975 \
	device/rockchip/rk3066/system/bin/wpa_supplicant:system/bin/wpa_supplicant \
	device/rockchip/rk3066/system/bin/ntfs-3g:system/bin/ntfs-3g
# alsa files
PRODUCT_COPY_FILES += device/rockchip/rk3066/system/usr/share/alsa/alsa.conf:system/usr/share/alsa/alsa.conf \
	device/rockchip/rk3066/system/usr/share/alsa/cards/aliases.conf:system/usr/share/alsa/cards/aliases.conf \
	device/rockchip/rk3066/system/usr/share/alsa/init/00main:system/usr/share/alsa/init/00main \
	device/rockchip/rk3066/system/usr/share/alsa/init/default:system/usr/share/alsa/init/default \
	device/rockchip/rk3066/system/usr/share/alsa/init/hda:system/usr/share/alsa/init/hda \
	device/rockchip/rk3066/system/usr/share/alsa/init/help:system/usr/share/alsa/init/help \
	device/rockchip/rk3066/system/usr/share/alsa/init/info:system/usr/share/alsa/init/info \
	device/rockchip/rk3066/system/usr/share/alsa/init/test:system/usr/share/alsa/init/test \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/center_lfe.conf:system/usr/share/alsa/pcm/center_lfe.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/default.conf:system/usr/share/alsa/pcm/default.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/dmix.conf:system/usr/share/alsa/pcm/dmix.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/dpl.conf:system/usr/share/alsa/pcm/dpl.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/dsnoop.conf:system/usr/share/alsa/pcm/dsnoop.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/front.conf:system/usr/share/alsa/pcm/front.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/iec958.conf:system/usr/share/alsa/pcm/iec958.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/modem.conf:system/usr/share/alsa/pcm/modem.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/rear.conf:system/usr/share/alsa/pcm/rear.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/side.conf:system/usr/share/alsa/pcm/side.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/surround40.conf:system/usr/share/alsa/pcm/surround40.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/surround41.conf:system/usr/share/alsa/pcm/surround41.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/surround50.conf:system/usr/share/alsa/pcm/surround50.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/surround51.conf:system/usr/share/alsa/pcm/surround51.conf \
	device/rockchip/rk3066/system/usr/share/alsa/pcm/surround71.conf:system/usr/share/alsa/pcm/surround71.conf

PRODUCT_COPY_FILES += \
	device/rockchip/rk3066/system/lib/hw/audio_policy.rk30board.so:system/lib/hw/audio_policy.rk30board.so \
    device/rockchip/rk3066/system/lib/hw/audio.primary.rk30board.so:system/lib/hw/audio.primary.rk30board.so \
    device/rockchip/rk3066/system/lib/hw/camera.rk30board.so:system/lib/hw/camera.rk30board.so \
    device/rockchip/rk3066/system/lib/hw/gpu.rk30board.so:system/lib/hw/gpu.rk30board.so \
    device/rockchip/rk3066/system/lib/hw/gralloc.rk30board.so:system/lib/hw/gralloc.rk30board.so \
    device/rockchip/rk3066/system/lib/hw/keystore.default.so:system/lib/hw/keystore.default.so \
    device/rockchip/rk3066/system/lib/hw/lights.rk30board.so:system/lib/hw/lights.rk30board.so \
    device/rockchip/rk3066/system/lib/hw/power.rk30xx.so:system/lib/hw/power.rk30xx.so \
    device/rockchip/rk3066/system/lib/hw/sensors.rk30board.so:system/lib/hw/sensors.rk30board.so \
	device/rockchip/rk3066/system/lib/hw/alsa.default.so:system/lib/hw/alsa.default.so
#	device/rockchip/rk3066/system/lib/hw/audio.a2dp.default.so:system/lib/hw/audio.a2dp.default.so \

PRODUCT_COPY_FILES += \
	device/rockchip/rk3066/system/lib/hw/hwcomposer.rk30board.so:system/lib/hw/hwcomposer.rk30board.so

PRODUCT_COPY_FILES += \
	device/rockchip/rk3066/system/app/RkVideoPlayer.apk:system/app/RkVideoPlayer.apk \
	device/rockchip/rk3066/system/app/RkExplorer.apk:system/app/RkExplorer.apk

PRODUCT_COPY_FILES += \
	device/rockchip/rk3066/system/media/bootanimation.zip:system/media/bootanimation.zip

PRODUCT_PACKAGES += \
	librs_jni \
	com.android.future.usb.accessory
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=160 \
    ro.com.google.networklocation=1 \
    dalvik.vm.lockprof.threshold=500 \
    dalvik.vm.dexopt-flags=m=y

PRODUCT_PROPERTY_OVERRIDES += \
	hwui.render_dirty_regions=false
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
	persist.sys.usb.otg=slave

PRODUCT_CHARACTERISTICS := tablet

DEVICE_PACKAGE_OVERLAYS := \
    device/rockchip/rk3066/overlay

PRODUCT_TAGS += dalvik.gc.type-precise

PRODUCT_PACKAGES += \
    Camera
PRODUCT_AAPT_CONFIG := xlarge mdpi normal xhdpi hdpi

PRODUCT_PACKAGES += \
	audio.a2dp.default \
	audio.usb.default \
	libavcodec \
    libavutil \
	make_ext4fs \
	hciconfig \
	hcitool \
	rk_afptool \
	tinyplay \
	rk_img_unpack \
	rk_img_maker \
	rk_mkkrnlimg \
	rk_mkbootimg

$(call inherit-product-if-exists, vendor/google/gapps.mk)
$(call inherit-product, frameworks/native/build/tablet-dalvik-heap.mk)
